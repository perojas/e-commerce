<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'example2',
            'displayname' => 'User Test',
            'firstname' => 'User',
            'lastname' => 'Test',
            'email' => 'example2@example.com',
            'password' => 'password',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('products')->insert([
            [
                'name' => 'Laptop HP 15-da0001la',
                'slug' => Str::slug('Laptop HP 15-da0001la'),
                'description' => '<ul><li class="processorfamily">Procesador Intel® Celeron®</li><li class="osinstalled">Windows 10 Home 64</li><li class="hd_01des">SATA de 500 GB, 5400 rpm</li><li class="memstdes_01">4 GB de SDRAM DDR4-2400 (1 x 4 GB)</li><li class="display-displaydes">Pantalla con retroiluminación WLED HD SVA BrightView de 15.6" en diagonal (1366 x 768)</li><li class="graphicseg_01card_01">Gráficos Intel® UHD 600</li></ul>',
                'quantity' => 20,
                'price' => 5999.00,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted' => false
            ],
            [
                'name' => 'Laptop HP - 15-dy0016la',
                'slug' => Str::slug('Laptop HP - 15-dy0016la'),
                'description' => '<ul><li class="processorfamily">Procesador de 7a generación Intel® Core™ i3</li><li class="osinstalled">Windows 10 Home 64</li><li class="hd_01des">Unidad de estado sólido PCIe® NVMe™ M.2 de 256 GB</li><li class="memstdes_01">12 GB de SDRAM DDR4-2133 (1 x 4 GB, 1 x 8 GB)</li><li class="display-displaydes">Pantalla BrightView con retroiluminación WLED HD SVA antirreflectante con microborde, de 15.6" en diagonal (1366&nbsp;x&nbsp;768)</li><li class="graphicseg_01card_01">Gráficos Intel® HD 620</li></ul>',
                'quantity' => 10,
                'price' => 999.00,
                'deleted' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Laptop HP ProBook 440 G6',
                'slug' => Str::slug('Laptop HP ProBook 440 G6'),
                'description' => '<ul><li class="processorfamily">Procesador Intel® Core™ i5 de 8° generación</li><li class="osinstalled">Windows 10 Pro 64</li><li class="hd_01des">SATA de 1 TB y 5400 rpm</li><li class="memstdes_01">8 GB de SDRAM DDR4-2400 (1 x 8 GB)</li><li class="display-displaydes">Pantalla con retroiluminación LED HD SVA eDP antirreflectante, de 14" en diagonal, 220&nbsp;cd/m², 67&nbsp;% de sRGB (1366&nbsp;x&nbsp;768)</li><li class="graphicseg_01card_01">Gráficos Intel® UHD 620</li></ul>',
                'quantity' => 10,
                'price' => 999.00,
                'deleted' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Smartphone Xiaomi Mi A1 dual Android one 7.1',
                'slug' => Str::slug('Smartphone Xiaomi Mi A1 dual Android one 7.1'),
                'description' => '<ul><li class="processorfamily">Procesador Intel® Core™ i5 de 8° generación</li><li class="osinstalled">Windows 10 Pro 64</li><li class="hd_01des">SATA de 1 TB y 5400 rpm</li><li class="memstdes_01">8 GB de SDRAM DDR4-2400 (1 x 8 GB)</li><li class="display-displaydes">Pantalla con retroiluminación LED HD SVA eDP antirreflectante, de 14" en diagonal, 220&nbsp;cd/m², 67&nbsp;% de sRGB (1366&nbsp;x&nbsp;768)</li><li class="graphicseg_01card_01">Gráficos Intel® UHD 620</li></ul>',
                'quantity' => 10,
                'price' => 999.00,
                'deleted' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
        DB::table('product_images')->insert([
            [
                'product_id'=> 1,
                'src'=>'https://d22k5h68hofcrd.cloudfront.net/catalog/product/cache/314dec89b3219941707ad62ccc90e585/3/P/3PX26LA-1_T1583186166.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'product_id' => 2,
                'src' => 'https://d22k5h68hofcrd.cloudfront.net/catalog/product/cache/314dec89b3219941707ad62ccc90e585/6/Q/6QV49LA-1_T1576679669.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'product_id' => 3,
                'src' => 'https://d22k5h68hofcrd.cloudfront.net/catalog/product/cache/314dec89b3219941707ad62ccc90e585/6/C/6CQ97LT-1_T1568310198.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'product_id' => 4,
                'src' => 'https://images-americanas.b2w.io/produtos/01/00/sku/29296/2/29296259G1.jp',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
