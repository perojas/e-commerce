# E-commerce

## Laravel + Vuetify + Laravel Passport + Swagger realtime starter app.


## Installation

1. clone `https://gitlab.com/perojas/e-commerce.git`
3. `composer install`
4. `npm install`

## Adding Swagger

- php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
- php artisan l5-swagger:generate


9. `php artisan migrate:fresh --seed`
10. node websocket
11. Serve Your Site

    - laravel valet - valet link YOURSITE
    - laradock - docker-compose up -d nginx redis mysql
    - homestead - homestead up
      [yourprojectname.test/](yourprojectname.test)

## Adding Git Commit Hooks

- added pre-commit hook file use this type : `cp pre-commit .git/hooks/pre-commit` to use this.
- make sure it is executable on your system
  `chmod +x .git/hooks/pre-commit`

## Stacks Used

- Laravel
- Axios 
- Vue
- Vuex
- Laravel Passport for Oauth and authentication
- Vuetify
- VueRouter

## Plugins

Here you can add extra plugins to your app:

## Theme

```

themes: {
        light: {
            primary: "#4682b4",
            secondary: "#b0bec5",
            accent: "#8c9eff",
            error: "#b71c1c"
        }
}

```

