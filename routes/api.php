<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'auth:api'
], function() {

    //Products
    Route::get('products/{id}','Rest\ProductsRestController@GetOne');
    Route::put('products','Rest\ProductsRestController@Put');
    Route::post('products','Rest\ProductsRestController@Post');
    Route::delete('products/{id}','Rest\ProductsRestController@Delete');

    //Users
    Route::get('users','Rest\UsersRestController@Get');
    Route::get('users/whoami','Rest\UsersRestController@WhoAmI');
    Route::get('users/data/{id}','Rest\UsersRestController@GetOne');
    Route::put('users','Rest\UsersRestController@Put');
    Route::post('users','Rest\UsersRestController@Post');
    Route::delete('users/{id}','Rest\UsersRestController@Delete');

    //Orders
    Route::get('orders','Rest\OrdersRestController@Get');
    Route::get('orders/{id}','Rest\OrdersRestController@GetOne');
    Route::put('orders','Rest\OrdersRestController@Put');
    Route::post('orders','Rest\OrdersRestController@Post');
    Route::delete('orders/{id}','Rest\OrdersRestController@Delete');

    //Images
    Route::put('products/{id}/image','Rest\ImageRestController@PutImage');
    Route::get('products/{id}/images','Rest\ImageRestController@Get');


});
Route::group(['middleware' => 'guest:api'], function () {

});

//Products
Route::get('products','Rest\ProductsRestController@Get');

//Shopping Cart
Route::get('shoppingcart','Rest\ShoppingCartRestController@Get');
Route::put('shoppingcart','Rest\ShoppingCartRestController@Put');
Route::post('login', 'Auth\LoginController@login');
