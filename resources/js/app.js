/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue'
import App from "./App.vue";
import router from './router/index'
import store from './store/index'
import interceptorsSetup from './helpers/interceptors';
import VueNumerals from 'vue-numerals';
require('./bootstrap');

window.Vue = require('vue');
import Vuetify from 'vuetify';

import '@fortawesome/fontawesome-free/css/all.css'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import VueCkeditor from 'vue-ckeditor5'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'
import VueMask from 'di-vue-mask'
import 'vuetify/dist/vuetify.min.css'
import money from 'v-money'

interceptorsSetup();
const options = {
    editors: {
        classic: ClassicEditor,
    },
    name: 'ckeditor'
};

export default new Vuetify({
    icons: {
        iconfont: "md" // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
    },
    theme: {
        dark: false
    },
    themes: {
        light: {
            primary: "#4682b4",
            secondary: "#b0bec5",
            accent: "#8c9eff",
            error: "#b71c1c"
        }
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(money, {precision: 4});
Vue.use(Vuetify);
Vue.use(Vuetify, {
    iconfont: 'fa'
});
Vue.use(VueMask);
Vue.use(VueAwesomeSwiper, /* { default options with global component } */);
Vue.use(VueCkeditor.plugin, options);
Vue.use(VueNumerals);

window.vm = new Vue({
    el: "#app",
    store,
    router,
    vuetify: new Vuetify(),
    render: h => h(App)
});
//Vue.prototype.$utils = utils
