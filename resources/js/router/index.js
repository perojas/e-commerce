import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './../store/index'

// Layouts
const LayoutAuth = () => import(/* webpackChunkName: "view-private-layout" */ '../views/layouts/LayoutAuth.vue');
const LayoutPublic = () => import(/* webpackChunkName: "view-private-layout" */ '../views/layouts/LayoutPublic.vue');
const LayoutPrivate = () => import(/* webpackChunkName: "view-private-layout" */ '../views/layouts/LayoutPrivate.vue');

/*
 |--------------------------------------------------------------------------
 | Public Views
 |--------------------------------------------------------------------------|
 */
const Login = () => import(/* webpackChunkName: "view-private-layout" */ '../views/auth/login.vue');

// Product
const Products = () => import(/* webpackChunkName: "view-private-layout" */ '../views/public/product/list.vue');
const ProductDetail = () => import(/* webpackChunkName: "view-private-layout" */ '../views/public/product/detail.vue');
const ProductCart = () => import(/* webpackChunkName: "view-private-layout" */ '../views/public/product/cart.vue')


/*
 |--------------------------------------------------------------------------
 | Private Views
 |--------------------------------------------------------------------------|
 */

// Product
const ProductNew = () => import(/* webpackChunkName: "view-private-layout" */ '../views/admin/product/add.vue');
const ProductEdit = () => import(/* webpackChunkName: "view-private-layout" */ '../views/admin/product/edit.vue');
const ProductList = () => import(/* webpackChunkName: "view-private-layout" */ '../views/admin/product/list.vue');

//User
const UserList = () => import(/* webpackChunkName: "view-private-layout" */ '../views/admin/user/list.vue');
const UserNew = () => import(/* webpackChunkName: "view-private-layout" */ '../views/admin/user/add.vue');
const UserEdit = () => import(/* webpackChunkName: "view-private-layout" */ '../views/admin/user/edit.vue');

Vue.use(VueRouter)


const authPages = {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {requiresAuth: false, transitionName: 'slide'},
};
const publicPages = {
    path: '/',
    component:LayoutPublic,
    children:[
        {
            path: '/',
            component: Products,
            name: 'Products',
            meta: {requiresAuth: false, transitionName: 'slide'},
        },
        {
            path: '/product-detail',
            component: ProductDetail,
            name: 'ProductDetail',
            meta: {requiresAuth: false, transitionName: 'slide'},
        },
        {
            path: '/shopping-cart',
            component: ProductCart,
            name: 'ProductCart',
            meta: {requiresAuth: false, transitionName: 'slide'},
        }
    ]
};

const privatePage = {
    path: '/admin',
    component:LayoutPrivate,
    redirect: '/product/list',
    children:[
        {
            path: 'product/list',
            component: ProductList,
            name: 'ProductList',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
        {
            path: 'product/new',
            component: ProductNew,
            name: 'ProductNew',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
        {
            path: 'product/:id/edit',
            component: ProductEdit,
            name: 'ProductEdit',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
        {
            path: '/product/new',
            component: ProductNew,
            name: 'ProductNew',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
        {
            path: 'user/list',
            component: UserList,
            name: 'UserList',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
        {
            path: 'user/new',
            component: UserNew,
            name: 'UserNew',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
        {
            path: 'user/:id/edit',
            component: UserEdit,
            name: 'UserEdit',
            meta: {requiresAuth: true, transitionName: 'slide'},
        },
    ]
}




const index = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'is-active',
    routes: [
        authPages,
        publicPages,
        privatePage,
        {path: '*', redirect: '/404', meta: {requiresAuth: false, transitionName: 'slide'}},
    ],
});

index.beforeEach((to, from, next) => {
    console.log('router',to,from,next)
    //  If the next route is requires user to be Logged IN
    if (to.matched.some(m => m.meta.requiresAuth)) {
        if (!store.getters['auth/isAuthenticated']) {
            next({
                path: '/login',
                query: {redirect: to.fullPath},
            });
        }
    }

    return next()
})

export default index;

