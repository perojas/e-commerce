import Vue from 'vue';
import Vuex from 'vuex';
import VueRx from 'vue-rx'
import product from "./modules/product";
import auth from "./modules/auth";
import user from "./modules/user";
import shoppingcart from "./modules/shoppingcart";
import order from "./modules/order";
import common from "./modules/common";
Vue.use(Vuex);


export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    getters:{},
    modules: {
        product: {
            namespaced: true,
            state: product.state,
            mutations: product.mutations,
            getters: product.getters,
            actions: product.actions,
        },
        auth: {
            namespaced: true,
            state: auth.state,
            mutations: auth.mutations,
            getters: auth.getters,
            actions: auth.actions,
        },
        user: {
            namespaced: true,
            state: user.state,
            mutations: user.mutations,
            getters: user.getters,
            actions: user.actions,
        },
        shoppingcart: {
            namespaced: true,
            state: shoppingcart.state,
            mutations: shoppingcart.mutations,
            getters: shoppingcart.getters,
            actions: shoppingcart.actions,
        },
        order: {
            namespaced: true,
            state: order.state,
            mutations: order.mutations,
            getters: order.getters,
            actions: order.actions,
        },
        common: {
            namespaced: true,
            state: common.state,
            mutations: common.mutations,
            getters: common.getters,
            actions: common.actions,
        }
    },
});

