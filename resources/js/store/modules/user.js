import Vue from 'vue';

import axios from "axios";
import {CURRENT_PRODUCT, DATA_PRODUCT, SELECT_PRODUCT} from "./product";

export const Api = 'http://localhost:8888/api'
export const ALL = 'ALL'
export const CURRENT_USER = 'CURRENT_USER'
export const DATA_USER = 'DATA_USER'

const state = {
    profile: {},
    findUser: undefined,
    status: '',
    users: [],
    currentUser: {},
    selectedUser: undefined,
}

const getters = {
    profile: userState => userState.profile,
    getAllUsers: state => state.users,
    getFindUser: state => state.findUser,
}

const actions = {
    userRequest ({ commit }, userData) {
        return new Promise((resolve, reject) => {
            commit('userRequest');
            axios.get('/api/users/whoami')
                .then(response => {
                    const data = response.data;
                    commit('userSuccess', { data })
                    resolve(response)
                })
                .catch(err => {
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    all ({ commit }, userData) {
        return new Promise((resolve, reject) => {
            axios.get('/api/users')
                .then(response => {
                    const users = response.data;
                    commit(ALL, { users })
                    resolve(response)
                })
                .catch(err => {
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    insert ({ commit, dispatch }, user) {
        return new Promise((resolve, reject) => {
            axios.put('/api/users',user)
                .then(response => {
                    const products = response.data;
                    //  commit(CURRENT_PRODUCT, { products })
                    resolve(response)
                })
                .catch(err => {
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    find ({ commit }, productId) {
        return new Promise((resolve, reject) => {
            let id = 0;
            if(state.currentUser instanceof jQuery) {
                id = state.currentUser.id
            }else{
                id = state.currentUser;
            }

            axios.get('/api/users/data/' + id)
                .then(response => {
                    const user = response.data;
                    if(state.currentUser instanceof jQuery) {}else{
                        commit(DATA_USER, user)
                    }
                    resolve(response)
                })
                .catch(err => {
                    console.log('login error')
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    delete ({ commit,dispatch }, userId) {
        return new Promise((resolve, reject) => {
            axios.delete('/api/products/' + productId)
                .then(response => {
                    dispatch('user/all', null, {root: true});
                    resolve(response)
                })
                .catch(err => {
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },

    currentUser ({ commit }, product) {
        commit(CURRENT_USER, product)
    },

    selectProduct ({ commit }, selectedProduct) {
        commit(SELECT_PRODUCT, selectedProduct)
    },

}

const mutations = {
    ALL: (state, { users }) => {
        state.users = users
    },
    CURRENT_USER: (state, user) => {
        state.currentUser = user;
    },
    SELECT_USER: (state, user) => {
        state.selectedUser = user
    },
    DATA_USER: (state, user) => {
        state.findUser = user;
    },
    userRequest (state) {
        state.status = 'attempting request for user profile data';
    },
    userSuccess(state, { data }) {
        state.status = 'success';
        state.profile = data;
    },
    userError (state) {
        state.status = 'error';
    },

}

export default {
    state,
    getters,
    actions,
    mutations,
};
