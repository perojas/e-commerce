import Vue from 'vue';

import axios from "axios";

export const Api = 'http://localhost:8888/api'
const state = {
    authStatus: '',
    token: localStorage.getItem('token') || '',
    user: {},
    tableList: []
}

const getters = {
    isAuthenticated:  state => !!state.token,
    authorized: state => !!state.token,
    authstatus: state => state.authStatus,
    authToken: authState => authState.token,
}

const actions = {
    login ({ commit,dispatch }, userData) {
        return new Promise((resolve, reject) => {
            commit('auth_request')
            axios.post('/oauth/token', userData)
                .then(response => {
                    const token = response.data.access_token
                    //const user = response.data.username
                    console.log(response)
                    // storing jwt in localStorage. https cookie is safer place to store
                    localStorage.setItem('token', token)

                  //  localStorage.setItem('user', user)
                    dispatch('shoppingcart/all', null, {root: true});
                    // mutation to change state properties to the values passed along
                    commit('auth_success', { token })
                    dispatch('user/userRequest', null, {root: true});
                    resolve(response)
                })
                .catch(err => {
                    console.log('login error')
                    commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
}


const mutations = {
    auth_request (state) {
        state.authStatus = 'loading'
    },
    auth_success (state, { token }) {
        state.authStatus = 'success'
        state.token = token
    },
    auth_error (state) {
        state.authStatus = 'error'
    },
    logout (state) {
        state.authStatus = ''
        state.token = ''
    },
    setTableList (state, tableList) {
        state.tableList = tableList
    }
}


export default {
    state,
    getters,
    actions,
    mutations,
};
