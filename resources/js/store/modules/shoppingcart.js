import Vue from 'vue';

import axios from "axios";

export const Api = 'http://localhost:8888/api';
export const ADD = 'ADD';
export const ALL = 'ALL';

const state = {
    cartProducts: [],
}

const getters = {
    getProductsInCart: state => state.cartProducts,
}

const actions = {
    all ({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('/api/shoppingcart')
                .then(response => {
                    const products = response.data;
                    commit(ALL, { products });
                    resolve(response)
                })
                .catch(err => {
                    console.log('login error')
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    add ({ commit }, product) {
        return new Promise((resolve, reject) => {
            axios.put('/api/shoppingcart',product)
                .then(response => {
                    const add = response.data;
                    commit(ADD, { add });
                    resolve(response)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
}

const mutations = {
    ALL: (state, { products }) => {
        state.cartProducts = [];
        state.cartProducts = products ;
    },
    ADD: (state, { add }) => {
        state.cartProducts.push(add);
    },
}


export default {
    state,
    getters,
    actions,
    mutations,
};
