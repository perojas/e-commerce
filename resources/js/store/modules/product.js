import Vue from 'vue';

import axios from "axios";

export const Api = 'http://localhost:8888/api'
export const ALL = 'ALL'
export const CURRENT_PRODUCT = 'CURRENT_PRODUCT'
export const DATA_PRODUCT = 'DATA_PRODUCT'
export const SELECT_PRODUCT = 'SELECT_PRODUCT'

const state = {
    products: [],
    findProduct: undefined,
    cartProducts: [],
    currentProduct: {},
    selectedProduct: undefined,
    showModal: false,
    showPopupCart: false,
    showPopupDelete: false,
    showPopupLogin: false,
}

const getters = {
    getNotebooks: state => state.notebooks,
    getSmartphones: state => state.smartphones,
    getAllProducts: state => state.products,
    getProductsInCart: state => state.cartProducts,
    getCurrentProduct: state => state.currentProduct,
    getFindProduct: state => state.findProduct,
    getShowModal: state => state.showModal,
    getPopupCart: state => state.showPopupCart,
    getPopupLogin: state => state.showPopupLogin,
    getPopupDelete: state => state.showPopupDelete,
}

const actions = {
    all ({ commit }, userData) {
        return new Promise((resolve, reject) => {
            axios.get('/api/products')
                .then(response => {
                    const products = response.data;
                    commit(ALL, { products })
                    resolve(response)
                })
                .catch(err => {
                    console.log('login error')
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    find ({ commit }, productId) {
        return new Promise((resolve, reject) => {
            let id = 0;
            if(state.currentProduct instanceof jQuery) {
                id = state.currentProduct.id
            }else{
                id = state.currentProduct;
            }

            console.log('find',state.currentProduct.id)
            axios.get('/api/products/' + id)
                .then(response => {
                    const product = response.data;
                    if(state.currentProduct instanceof jQuery) {}else{
                        commit(DATA_PRODUCT, product)
                    }
                    resolve(response)
                })
                .catch(err => {
                    console.log('login error')
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    insert ({ commit, dispatch }, product) {
        return new Promise((resolve, reject) => {
            console.log('find',product)
            axios.put('/api/products',product)
                .then(response => {
                    const products = response.data;
                    //  commit(CURRENT_PRODUCT, { products })
                    resolve(response)
                })
                .catch(err => {
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    update ({ commit, dispatch }, product) {
        return new Promise((resolve, reject) => {
            axios.post('/api/products',product)
                .then(response => {
                    const products = response.data;
                    //  commit(CURRENT_PRODUCT, { products })
                    resolve(response)
                })
                .catch(err => {
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    delete ({ commit,dispatch }, productId) {
        return new Promise((resolve, reject) => {
            axios.delete('/api/products/' + productId)
                .then(response => {
                    dispatch('product/all', null, {root: true});
                    resolve(response)
                })
                .catch(err => {
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },

    currentProduct ({ commit }, product) {
        commit(CURRENT_PRODUCT, product)
    },

    selectProduct ({ commit }, selectedProduct) {
        commit(SELECT_PRODUCT, selectedProduct)
    },
    addProduct: (context, product) => {
        console.log("data",context,product);
        context.commit('ADD_PRODUCT', product);
    },
    removeProduct: (context, index) => {
        context.commit('REMOVE_PRODUCT', index);
    },
    /*currentProduct: (context, product) => {
        context.commit('CURRENT_PRODUCT', product);
    },*/
    showOrHiddenModal: (context) => {
        context.commit('SHOW_MODAL');
    },
    showOrHiddenPopupCart: (context) => {
        context.commit('SHOW_POPUP_CART');
    },
    showOrHiddenPopupLogin: (context) => {
        context.commit('SHOW_POPUP_LOGIN');
    },
    showOrHiddenPopupDelete: (context) => {
        context.commit('SHOW_POPUP_DELETE');
    },
}

const mutations = {
    ALL: (state, { products }) => {
        state.products = products
    },
    SELECT_PRODUCT: (state, selectedProduct) => {
        state.selectedProduct = selectedProduct
    },
    SHOW_POPUP_CART: (state) => {
        state.showPopupCart = !state.showPopupCart;
    },
    SHOW_POPUP_LOGIN: (state) => {
        state.showPopupLogin = !state.showPopupLogin;
    },
    SHOW_POPUP_DELETE: (state) => {
        state.showPopupDelete = !state.showPopupDelete;
    },


    ADD_PRODUCT: (state, product) => {
        state.cartProducts.push(product);
    },
    REMOVE_PRODUCT: (state, index) => {
        state.cartProducts.splice(index, 1);
    },
    CURRENT_PRODUCT: (state, product) => {
        state.currentProduct = product;
    },
    DATA_PRODUCT: (state, product) => {
        state.findProduct = product;
    },
    SHOW_MODAL: (state) => {
        state.showModal = !state.showModal;
    },

}

export default {
    state,
    getters,
    actions,
    mutations,
};
