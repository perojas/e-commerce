import Vue from 'vue';

import axios from "axios";

export const Api = 'http://localhost:8888/api';
export const ADD = 'ADD';
export const ALL = 'ALL';

const state = {

}

const getters = {

}

const actions = {
    insert ({ commit, dispatch }, order) {
        return new Promise((resolve, reject) => {
            axios.put('/api/orders',order)
                .then(response => {
                    const products = response.data;
                    //  commit(CURRENT_PRODUCT, { products })
                    resolve(response)
                })
                .catch(err => {
                    //commit('auth_error')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
}

const mutations = {

}

export default {
    state,
    getters,
    actions,
    mutations,
};
