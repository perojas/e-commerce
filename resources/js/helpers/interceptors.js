import store from './../store/index'
import axios from 'axios';

export default function interceptors() {
    axios.interceptors.request.use(function(config) {
        const authToken = store.getters['auth/authToken'];
        console.log('entro');
        if (authToken) {
            config.headers.Authorization = `Bearer ${authToken}`;
        }
        return config;
    }, function(err) {
        return Promise.reject(err);
    });
}
