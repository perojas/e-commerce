<?php


namespace App\Http\Controllers\Rest;


use App\Model\Repositories\ShoppingCart\Interfaces\ShoppingCartRepositoryInterface;
use App\Model\Repositories\ShoppingCart\ShoppingCartRepository;
use Illuminate\Http\Request;

class ShoppingCartRestController
{
    /**
     * @var ShoppingCartRepositoryInterface
     */
    private $shoppingCartRepository;

    /**
     * OrderRestController constructor.
     * @param ShoppingCartRepositoryInterface $shoppingCartRepository
     */
    public function __construct(
        ShoppingCartRepositoryInterface $shoppingCartRepository
    ){
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    /**
     * Display a listing of the order.
     *
     */
    /**
     * @OA\Get(
     *     tags={"ShoppingCart"},
     *     path="/api/shoppingcart",
     *     summary="Get list of shopping cart",
     *     @OA\Response(
     *         response=200,
     *         description="Get list of cart",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Get()
    {
        $shoppingCart = $this->shoppingCartRepository->findAll();

        return $shoppingCart;
    }

    /**
     * Display the specified order.
     *
     * @param  int $orderId
     */
    /**
     * @OA\Get(
     *     tags={"ShoppingCart"},
     *     path="/api/shoppingcart/{id}",
     *     summary="Get order by id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of order to return",
     *         in="path",
     *         name="shoppingcartId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of orders",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function GetOne($shoppingcartId)
    {
        $shoppingcart = $this->shoppingCartRepository->findOne($shoppingcartId);

        return $shoppingcart;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Put(
     *     tags={"ShoppingCart"},
     *     path="/api/shoppingcart",
     *     summary="New shoppingcart",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                   @OA\Property(
     *                     property="productId",
     *                     type="int32"
     *                 ),
     *                  @OA\Property(
     *                     property="total_products",
     *                     type="int32"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of order",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Put(Request $request)
    {
        $shoppingcart = $this->shoppingCartRepository->create($request);

        return $shoppingcart;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Post(
     *     tags={"ShoppingCart"},
     *     path="/api/shoppingcart",
     *     summary="Update order",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                  @OA\Property(
     *                     property="productId",
     *                     type="int32"
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="int32"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of order",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Post(Request $request)
    {
        $shoppingcart = $this->shoppingCartRepository->update($request);

        return $shoppingcart;
    }

    /**
     * Delete the specified order.
     *
     * @param  int $shoppingCartId
     */
    /**
     * @OA\Delete(
     *     tags={"ShoppingCart"},
     *     path="/api/shoppingcart/{id}",
     *     summary="Delete order by Id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of order to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of order",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function delete($id)
    {
        $products = $this->shoppingCartRepository->delete($id);

        return $products;
    }

}
