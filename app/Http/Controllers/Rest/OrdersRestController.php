<?php


namespace App\Http\Controllers\Rest;

use App\Model\Repositories\Orders\Interfaces\OrderRepositoryInterface;
use App\Model\Repositories\Orders\OrdersRepository;
use App\Model\Repositories\Products\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class OrdersRestController
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;


    /**
     * OrderRestController constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository
    ){
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the order.
     *
     */
    /**
     * @OA\Get(
     *     tags={"Orders"},
     *     path="/api/orders",
     *     summary="Get list of orders",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Response(
     *         response=200,
     *         description="Get list of orders",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Get()
    {
        $products = $this->orderRepository->findAll();

        return $products;
    }

    /**
     * Display the specified order.
     *
     * @param  int $orderId
     */
    /**
     * @OA\Get(
     *     tags={"Orders"},
     *     path="/api/orders/{id}",
     *     summary="Get order by id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of order to return",
     *         in="path",
     *         name="orderId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of orders",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function GetOne($orderId)
    {
        $orders = $this->orderRepository->findOne($orderId);

        return $orders;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Put(
     *     tags={"Orders"},
     *     path="/api/orders",
     *     summary="New order",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                   @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     type="double"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of order",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Put(Request $request)
    {
        $products = $this->orderRepository->create($request);

        return $products;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Post(
     *     tags={"Orders"},
     *     path="/api/orders",
     *     summary="Update order",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                  @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     type="double"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of order",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Post(Request $request)
    {
        $products = $this->orderRepository->update($request);

        return $products;
    }

    /**
     * Delete the specified order.
     *
     * @param  int $productId
     */
    /**
     * @OA\Delete(
     *     tags={"Orders"},
     *     path="/api/orders/{id}",
     *     summary="Delete order by Id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of order to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of order",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function delete($id)
    {
        $products = $this->orderRepository->delete($id);

        return $products;
    }
}
