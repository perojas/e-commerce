<?php


namespace App\Http\Controllers\Rest;


use App\Http\Controllers\Controller;
use App\Model\Repositories\ProductImage\Interfaces\ProductImageRepositoryInterface;
use App\Model\Repositories\Products\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ImageRestController extends Controller
{
    /**
     * @var ProductImageRepositoryInterface
     */
    private $imageRepository;

    /**
     * ProductsRestController constructor.
     * @param ImageRestController $imageRepository
     */

    public function __construct(
        ProductImageRepositoryInterface $imageRepository
    ){
        $this->imageRepository = $imageRepository;
    }

    /**
     * Display a listing of the products.
     *
     */
    /**
     * @OA\Get(
     *     tags={"Images"},
     *     path="/api/products/{id}/images",
     *     summary="Get list of products",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Get($productId)
    {
        $products = $this->imageRepository->findAll($productId);

        return $products;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Put(
     *     tags={"Images"},
     *     path="/api/products/{id}/image",
     *     summary="New product",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="productId",
     *                      type="integer",
     *                      format="int64"
     *                 ),
     *                 @OA\Property(
     *                      property="image",
     *                      type="array",
     *                      collectionFormat="multi",
     *
     *                      @OA\Items(type="file", format="file")
     *                  ),
     *                 required={"file"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function PutImage(Request $request,$productId)
    {
        if ($request->hasFile('image')) {
            $this->imageRepository->saveImages(collect($request->file('image')),$productId);
        }

        return true;
    }

}
