<?php

namespace App\Http\Controllers\Rest;

use App\Model\Repositories\Products\Interfaces\ProductRepositoryInterface;
use App\Model\Repositories\Products\ProductsRepository;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ProductsRestController extends Controller
{
     /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;


    /**
    * ProductsRestController constructor.
    * @param ProductRepositoryInterface $productRepository
    */

    public function __construct(
         ProductRepositoryInterface $productRepository
    ){
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the products.
     *
     */
    /**
     * @OA\Get(
     *     tags={"Products"},
     *     path="/api/products",
     *     summary="Get list of products",
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Get()
    {
        $products = $this->productRepository->findAll();

        return $products;
    }

    /**
     * Display the specified product.
     *
     * @param  int $productId
     */
    /**
     * @OA\Get(
     *     tags={"Products"},
     *     path="/api/products/{id}",
     *     summary="Get product by id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of product to return",
     *         in="path",
     *         name="productId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function GetOne($productId)
    {
        $products = $this->productRepository->findOne($productId);

        return $products;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Put(
     *     tags={"Products"},
     *     path="/api/products",
     *     summary="New product",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                   @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="slug",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="double"
     *                 ),
     *                  @OA\Property(
     *                     property="quantity",
     *                     type="integer"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Put(Request $request)
    {
        $product = $this->productRepository->create($request);

        return $product;
    }

    /**
     * @param Request $request
     */
    /**
     * @OA\Post(
     *     tags={"Products"},
     *     path="/api/products",
     *     summary="Update product",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                  @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     type="double"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Post(Request $request)
    {
        $products = $this->productRepository->update($request);

        return $products;
    }

    /**
     * Delete the specified product.
     *
     * @param  int $productId
     */
    /**
     * @OA\Delete(
     *     tags={"Products"},
     *     path="/api/products/{id}",
     *     summary="Delete product by Id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of product to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function delete($id)
    {
        $products = $this->productRepository->delete($id);

        return response()->json($products);
    }
}
