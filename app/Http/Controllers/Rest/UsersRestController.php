<?php

namespace App\Http\Controllers\Rest;

use App\Model\Repositories\Products\ProductsRepository;
use App\Model\Repositories\Users\UsersRepository;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersRestController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $usersRepository;

    public function __construct(
        UsersRepository $usersRepository
    ){
        $this->usersRepository = $usersRepository;
    }

    /**
     * Display a listing of the users.
     *
     */
    /**
     * @OA\Get(
     *     tags={"Users"},
     *     path="/api/users",
     *     summary="Get list of users",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Response(
     *         response=200,
     *         description="Get list of users",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Get()
    {
        $users = $this->usersRepository->FindAll();

        return $users;
    }

    /**
     * Display the specified product.
     *
     * @param  int $userId
     */
    /**
     * @OA\Get(
     *     tags={"Products"},
     *     path="/api/users/data/{id}",
     *     summary="Get product by id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of product to return",
     *         in="path",
     *         name="productId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function GetOne($userId)
    {
        $products = $this->usersRepository->findOne($userId);

        return $products;
    }

    /**
     * Display the specified user.
     *
     */
    /**
     * @OA\Get(
     *     tags={"Users"},
     *     path="/api/users/whoami",
     *     summary="Get user by id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function WhoAmI(Request $request)
    {
        return $request->user();
    }

    /**
     * @OA\Put(
     *     tags={"Users"},
     *     path="/api/users",
     *     summary="New user",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                   @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="displayname",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="firstname",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="lastname",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Put(Request $request)
    {
        $users = $this->usersRepository->Create($request);

        return $users;
    }

    /**
     * @OA\Post(
     *     tags={"Users"},
     *     path="/api/users",
     *     summary="Update user",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                  @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="displayname",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="firstname",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="lastname",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Post(Request $request)
    {
        $products = $this->usersRepository->Update($request);

        return $products;
    }

    /**
     * @OA\Delete(
     *     tags={"Users"},
     *     path="/api/users/{id}",
     *     summary="Delete user by Id",
     *     security={
     *         {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         description="ID of user to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get list of products",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *      )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized action"
     *     )
     * )
     */
    public function Delete($id)
    {
        $products = $this->UsersRepository->Delete($id);

        return $products;
    }

}
