<?php


namespace App\Model\Repositories\ShoppingCart\Interfaces;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Model\Entities\ShoppingCart;
use Illuminate\Support\Collection;

interface ShoppingCartRepositoryInterface
{
    public function findAll() : Collection ;

    public function findOne(int $id) : ShoppingCart;

    public function create($shoppingcart) : ShoppingCart;

    public function update($shoppingcart, $cart = false) : bool;

    public function delete() : bool;
}
