<?php


namespace App\Model\Repositories\ShoppingCart;

use App\Http\Controllers\Controller;
use App\Model\Entities\ShoppingCart;
use App\Model\Repositories\ShoppingCart\Interfaces\ShoppingCartRepositoryInterface;
use Illuminate\Support\Collection;

class ShoppingCartRepository implements ShoppingCartRepositoryInterface
{
    /**
     * Return all the orders
     *
     * @return Collection
     */
    public function findAll() : Collection
    {
        $shopping = ShoppingCart::with('product.images')->where("status",0)->get();

        return $shopping;
    }


    /**
     * @param int $id
     * @return Order
     */
    public function findOne(int $id) : ShoppingCart
    {
        return ShoppingCart::where('id',$id)->with('product.images')->first();
    }

    /**
     * Create the order
     *
     * @param ShoppingCart $shoppingcart
     * @return ShoppingCart
     */
    public function create($shoppingcart) : ShoppingCart
    {

        $item = ShoppingCart::where('product_id',$shoppingcart->id)->first();
        if($item !== null){
            $item->total_products = $item->total_products + 1;
            $this->update($item,true);
            return $this->findOne($item->id);
        }

        $newItem = new ShoppingCart();
        $newItem->product_id = $shoppingcart->id;
        $newItem->total_products = 1;
        $newItem->status = 0;
        $newItem->created_at = date('Y-m-d H:i:s');
        if($newItem->save()){
            return $this->findOne($newItem->id);
        }


    }

    /**
     * Update the order
     *
     * @param Order $order
     * @return bool
     */
    public function update($shoppingcart, $cart = false) : bool
    {
        if(!$cart) {
            $updateItem = ShoppingCart::find($shoppingcart->id);
        }else {
            $updateItem = $shoppingcart;
        }

        if($updateItem != null){
            $updateItem->status = 0;
            $updateItem->product_id = $shoppingcart->product_id;
            $updateItem->total_products = $shoppingcart->total_products;
            $updateItem->updated_at = date('Y-m-d H:i:s');
            return $updateItem->update();
        }

        return false;

    }

    /**
     * Update the order
     *
     * @param Order $order
     * @return bool
     */
    public function updateCart($shoppingcart) : bool
    {

        if($shoppingcart != null){
            $shoppingcart->product_id = $shoppingcart->product_id;
            $shoppingcart->total_products = $shoppingcart->total_products;
            $shoppingcart->status = $shoppingcart->status;
            $shoppingcart->updated_at = date('Y-m-d H:i:s');
            return $shoppingcart->update();
        }

        return false;

    }

    /**
     * @return bool
     */
    public function delete() : bool
    {
        $updateItem = ShoppingCart::find($this->model->id);
        if($updateItem != null){
            return $updateItem->delete();
        }
        return false;
    }
}
