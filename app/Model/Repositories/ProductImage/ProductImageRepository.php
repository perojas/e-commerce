<?php


namespace App\Model\Repositories\ProductImage;


use App\Model\Entities\Product;
use App\Model\Entities\ProductImage;
use App\Model\Repositories\ProductImage\Interfaces\ProductImageRepositoryInterface;
use App\Tools\UploadableTrait;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\Storage;

class ProductImageRepository implements ProductImageRepositoryInterface
{
    use UploadableTrait;

    private $productId;

    /**
     * Return all the products
     *
     * @return Collection
     */
    public function findAll($productId) : Collection
    {
        $image = ProductImage::where("product_id",$productId)->get();

        $image->each(function($image, $key) {
            $image->src = url(Storage::url($image->src));
        });

        return $image;
    }

    /**
     * @param Collection $collection
     *
     * @return void
     */
    public function saveImages(Collection $collection, $id)
    {
        $this->productId = $id;
        $collection->each(function (UploadedFile $file) {
            $filename = $this->storeFile($file);
            $productImage = new ProductImage([
                'product_id' => $this->productId,
                'src' => $filename
            ]);
            $this->create($productImage);
        });
    }

    /**
     * Create the product
     *
     * @param Product $product
     * @return Product
     */
    public function create($image) : ProductImage
    {
        $newItem = new ProductImage();
        $newItem->product_id = $image->product_id;
        $newItem->src = $image->src;
        $newItem->created_at = date('Y-m-d H:i:s');
        $newItem->save();

        return $newItem;
    }

    /**
     * @return bool
     */
    public function delete($imageId) : bool
    {
        $updateItem = ProductImage::find($imageId);
        if($updateItem != null){
            if($updateItem->delete()){
                Storage::delete($updateItem->src);
            }
        }
        return false;
    }
}
