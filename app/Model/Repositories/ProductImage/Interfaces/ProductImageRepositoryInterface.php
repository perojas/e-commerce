<?php


namespace App\Model\Repositories\ProductImage\Interfaces;


use Illuminate\Support\Collection;

interface ProductImageRepositoryInterface
{
    public function findAll($productId) : Collection ;

    public function saveImages(Collection $collection,$productId);

    public function delete($imageId) : bool;
}
