<?php


namespace App\Model\Repositories\Users\Interfaces;


use App\User;
use Illuminate\Support\Collection;

interface UserRepositoryInterface
{
    public function findAll() : Collection ;

    public function findOne(int $id) : User;

    public function create($product) : User;

    public function update($product) : bool;

    public function delete() : bool;
}
