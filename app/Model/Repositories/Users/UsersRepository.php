<?php
namespace App\Model\Repositories\Users;
use App\Model\Entities\Product;
use App\Model\Repositories\Users\Interfaces\UserRepositoryInterface;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class UsersRepository implements UserRepositoryInterface
{
    /**
     * Return all the users
     *
     * @return Collection
     */
    public function findAll() : Collection {
        return User::all();
    }

    /**
     * @param int $id
     * @return Product
     */
    public function findOne(int $id) : User{
        return User::find($id)->first();
    }

    /**
     * Create the user
     *
     * @param User $user
     * @return Product
     */
    public function create($user) : User {
        $splitUsername = explode('@', $user->email);

        $newItem = new User();
        $newItem->username = $splitUsername[0];
        $newItem->displayname = $user->firstname;
        $newItem->firstname = $user->firstname;
        $newItem->lastname = $user->lastname;
        $newItem->email = $user->email;
        $newItem->password = Hash::make($user->password);
        $newItem->created_at = date('Y-m-d H:i:s');
        $newItem->save();

        return $newItem;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update($user) : bool {
        $updateItem = Product::find($user->id);
        if($updateItem != null){
            $updateItem->id = $user->id;
            $updateItem->displayname = $user->displayname;
            $updateItem->firstname = $user->firstname;
            $updateItem->lastname = $user->lastname;
            $updateItem->password = Hash::make($user->password);
            return $updateItem->update();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete() : bool
    {
        $updateItem = Product::find($this->model->id);
        if($updateItem != null){
            return $updateItem->delete();
        }
        return false;
    }
}
