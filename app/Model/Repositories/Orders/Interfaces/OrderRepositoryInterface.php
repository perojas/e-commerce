<?php


namespace App\Model\Repositories\Orders\Interfaces;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Model\Entities\Order;
use Illuminate\Support\Collection;

interface OrderRepositoryInterface
{
    public function findAll() : Collection ;

    public function findOne(int $id) : Order;

    public function create($order) : Order;

    public function update($order) : bool;

    public function delete() : bool;
}
