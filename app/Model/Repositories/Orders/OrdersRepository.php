<?php


namespace App\Model\Repositories\Orders;
use App\Model\Entities\Order;
use App\Model\Entities\Product;
use App\Model\Entities\ShoppingCart;
use App\Model\Repositories\Orders\Interfaces\OrderRepositoryInterface;
use App\Model\Repositories\ShoppingCart\Interfaces\ShoppingCartRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Str as Str;
use Jsdecena\Baserepo\BaseRepository;

class OrdersRepository implements OrderRepositoryInterface
{
    /**
     * @var ShoppingCartRepositoryInterface
     */
    private $shoppingCartRepository;

    /**
     * OrderRestController constructor.
     * @param ShoppingCartRepositoryInterface $shoppingCartRepository
     */
    public function __construct(
        ShoppingCartRepositoryInterface $shoppingCartRepository
    ){
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    /**
     * Return all the orders
     *
     * @return Collection
     */
    public function findAll() : Collection
    {
        return Order::where("deleted",false)->get();
    }

    /**
     * @param int $id
     * @return Order
     */
    public function findOne(int $id) : Order
    {
        return Order::where('id',$id)->first();
    }

    /**
     * Create the order
     *
     * @param Order $order
     * @return Order
     */
    public function create($order) : Order
    {
        $total_product = 0;
        $shoppingcart = ShoppingCart::with('product.images')->get();
        foreach ($shoppingcart as $shopping){
            $total_product += (int)$shopping->total_products * $shopping->product->price;
        }

        $newItem = new Order();
        $newItem->reference = rand (1, 10);
        $newItem->firstname = $order->firstname;
        $newItem->lastname = $order->lastname;
        $newItem->email = $order->email;
        $newItem->address = $order->address;
        $newItem->country = $order->country;
        $newItem->state = $order->state;
        $newItem->city = $order->city;
        $newItem->code = $order->code;
        $newItem->total_products = $shoppingcart->count();
        $newItem->total = $total_product;
        $newItem->status = 1;
        $newItem->created_at = date('Y-m-d H:i:s');
       if($newItem->save()){
           foreach ($shoppingcart as $shopping){
               $shopping->status = 1;
               $this->shoppingCartRepository->updateCart($shopping);
           }
       }

        return $newItem;
    }

    /**
     * Update the order
     *
     * @param Order $order
     * @return bool
     */
    public function update($order) : bool
    {
        $updateItem = Order::find($order->id);
        if($updateItem != null){
            $updateItem->reference = rand (1, 10);
            $updateItem->firstname = $order->firstname;
            $updateItem->lastname = $order->lastname;
            $updateItem->email = $order->email;
            $updateItem->address = $order->address;
            $updateItem->country = $order->country;
            $updateItem->state = $order->state;
            $updateItem->city = $order->city;
            $updateItem->code = $order->code;
            $updateItem->total_products = $order->total_products;
            $updateItem->total = $order->total;
            $updateItem->status = $order->status;
            $updateItem->updated_at = date('Y-m-d H:i:s');
            return $updateItem->update();
        }

        return false;

    }

    /**
     * @return bool
     */
    public function delete() : bool
    {
        $updateItem = Product::find($this->model->id);
        if($updateItem != null){
            return $updateItem->delete();
        }
        return false;
    }
}
