<?php


namespace App\Model\Repositories\Products\Interfaces;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Model\Entities\Product;
use Illuminate\Support\Collection;

interface ProductRepositoryInterface
{
    public function findAll() : Collection ;

    public function findOne(int $id) : Product;

    public function create($product) : Product;

    public function update($product) : Product;

    public function delete(int $id) : bool;

}
