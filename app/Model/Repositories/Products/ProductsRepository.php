<?php
namespace App\Model\Repositories\Products;

use App\Model\Entities\Product;
use App\Model\Entities\ProductImage;
use App\Model\Repositories\Products\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Str as Str;
use App\Tools\UploadableTrait;
use Jsdecena\Baserepo\BaseRepository;

class ProductsRepository implements ProductRepositoryInterface
{
    use UploadableTrait;

    /**
     * Return all the products
     *
     * @return Collection
     */
    public function findAll() : Collection
    {
        return Product::with('images')->where("deleted",false)->get();
    }

    /**
     * @param int $id
     * @return Product
     */
    public function findOne(int $id) : Product
    {
        return Product::find($id)->with('images')->where("deleted",false)->first();
    }

    /**
     * Create the product
     *
     * @param Product $product
     * @return Product
     */
    public function create($product) : Product
    {
        $newItem = new Product();
        $newItem->name = $product->name;
        $newItem->slug = Str::slug($product->name);
        $newItem->description = $product->description;
        $newItem->quantity = $product->quantity;
        $newItem->price = $product->price;
        $newItem->deleted = false;
        $newItem->created_at = date('Y-m-d H:i:s');
        $newItem->save();

        return $newItem;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function update($product) : Product
    {
        $updateItem = Product::find($product->id);
        if($updateItem != null){
            $updateItem->id = $product->id;
            $updateItem->name = $product->name;
            $updateItem->slug = Str::slug($product->name);
            $updateItem->description = $product->description;
            $updateItem->quantity = $product->quantity;
            $updateItem->price = $product->price;
            $updateItem->update();
            return $updateItem;
        }

        return $updateItem;

    }

    /**
     * @return bool
     */
    public function delete(int $id) : bool
    {
        $updateItem = Product::find($id);
        if($updateItem != null){
            $updateItem->deleted = true;
            $updateItem->updated_at = date('Y-m-d H:i:s');
            $updateItem->update();
            return true;
        }
        return false;
    }


}
