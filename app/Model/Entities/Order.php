<?php


namespace App\Model\Entities;


use Illuminate\Database\Eloquent\Model;

class Order  extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'reference', 'firstname', 'lastname', 'email', 'address', 'country', 'state', 'city', 'code', 'total_products', 'total', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at','updated_at'
    ];
}
