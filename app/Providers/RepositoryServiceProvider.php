<?php


namespace App\Providers;


use App\Model\Repositories\Orders\Interfaces\OrderRepositoryInterface;
use App\Model\Repositories\Orders\OrdersRepository;
use App\Model\Repositories\ProductImage\Interfaces\ProductImageRepositoryInterface;
use App\Model\Repositories\ProductImage\ProductImageRepository;
use App\Model\Repositories\Products\Interfaces\ProductRepositoryInterface;
use App\Model\Repositories\Products\ProductsRepository;
use App\Model\Repositories\ShoppingCart\Interfaces\ShoppingCartRepositoryInterface;
use App\Model\Repositories\ShoppingCart\ShoppingCartRepository;

use App\Model\Repositories\Users\Interfaces\UserRepositoryInterface;
use App\Model\Repositories\Users\UsersRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductsRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UsersRepository::class
        );

        $this->app->bind(
            ProductImageRepositoryInterface::class,
            ProductImageRepository::class
        );
        $this->app->bind(
            OrderRepositoryInterface::class,
            OrdersRepository::class
        );
        $this->app->bind(
            ShoppingCartRepositoryInterface::class,
            ShoppingCartRepository::class
        );
    }
}
